/*
Copyright (C) 2016 Chiara Segala, Octave Arena, <segalachiara92@gmail.com>
OdePkg - A package for solving ordinary differential equations and more

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

// mkoctfile taylor.cc
#include <octave/oct.h>
#include "xnorm.h"

template<class LMt, class RMt>
RMt&
taylor (const octave_idx_type s, const double t,
        const LMt &A, RMt &b, RMt &f,
        const bool full_term, const bool prnt,
        const octave_idx_type m, octave_idx_type mv,
        const double eta, const double tol)
{
  auto &inf = octave::numeric_limits<double>::Inf;
   
  for (int i = 1; i <= s; i++)
    {
      double c1 = xnorm (b, inf ());
      for (int k = 1; k <= m; k++) 
        {
          b = RMt ((t/(s*k))*(A*b));
          mv = mv + 1;
          f =  f + b;
          double c2 = xnorm (b, inf ());
          if (! full_term)
            {
              if (c1 + c2 <= tol*xnorm(f, inf ()))
                {
                  if (prnt)
                    {
                      printf(" %2d, ", k);
                      printf("break %e\n",c1+c2);
                    }
                  break;
                }
              c1 = c2;
            }
      
        }
      f = eta*f;
      b = f; 
    }
  return b;
};

DEFUN_DLD (__taylor__, args, ,
           "Computing the Action of the Matrix Exponential")
{

  octave_idx_type s  = args(0).idx_type_value ();
  double t           = args(1).double_value ();  
  bool full_term     = args(5).bool_value ();
  bool prnt          = args(6).bool_value ();
  octave_idx_type m  = args(7).idx_type_value ();
  octave_idx_type mv = args(8).idx_type_value ();
  double eta         = args(9).double_value ();
  double tol         = args(10).double_value ();

  octave_value_list retval;

#define MAKE_BRANCH(c21, c22, c23, c24, c31, c32, c33, c34, t1, t2, d2, d3) \
  if (c21 args(2).is_sparse_type ()                                     \
      c22 args(2).is_real_type ()                                       \
      c23 args(2).is_single_type ()                                     \
      c24 args(2).is_complex_type ()                                    \
      c31 args(3).is_sparse_type ()                                     \
      c32 args(3).is_real_type ()                                       \
      c33 args(3).is_single_type ()                                     \
      c34 args(3).is_complex_type ())                                   \
    {                                                                   \
      t1 A = args(2) . d2 ();                                           \
      t2 b = args(3) . d3 ();                                           \
      t2 f = args(4) . d3 ();                                           \
      retval(0) = taylor (s, t, A, b, f, full_term, prnt,               \
                          m, mv, eta, tol);                             \
    };
  //           c21 c22  c23    c24    c31    c32  c33    c34
  MAKE_BRANCH (! , && , && ! , && ! , && ! , && , && ! , && ! , Matrix,       Matrix,       matrix_value,        matrix_value);
  MAKE_BRANCH (  , && , && ! , && ! , &&   , && , && ! , && ! , SparseMatrix, SparseMatrix, sparse_matrix_value, sparse_matrix_value);
  MAKE_BRANCH (! , && , && ! , && ! , &&   , && , && ! , && ! , Matrix,       SparseMatrix, matrix_value,        sparse_matrix_value);
  MAKE_BRANCH (  , && , && ! , && ! , && ! , && , && ! , && ! , SparseMatrix, Matrix,       sparse_matrix_value, matrix_value);
  
  return retval;
}
