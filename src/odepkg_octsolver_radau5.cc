/*
Copyright (C) 2016, Andreas Pels <pels@gsc.tu-darmstadt.de>
Copyright (C) 2007-2012, Thomas Treichl <treichl@users.sourceforge.net>
OdePkg - A package for solving ordinary differential equations and more

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

//#include <config.h>
#include <oct.h>
#include <f77-fcn.h>
#include <oct-map.h>
#include <parse.h>
#include "odepkg_auxiliary_functions.h"

typedef octave_idx_type (*odepkg_radau5_usrtype)
  (const octave_idx_type& N, const double& X, const double* Y, double* F,
   OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR);

typedef octave_idx_type (*odepkg_radau5_jactype)
  (const octave_idx_type& N, const double& X, const double* Y, double* DFY,
   OCTAVE_UNUSED const octave_idx_type& LDFY,
   OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR);

typedef F77_RET_T (*odepkg_radau5_masstype)
  (const octave_idx_type& N, double* AM, 
   OCTAVE_UNUSED const octave_idx_type* LMAS,
   OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR);

typedef octave_idx_type (*odepkg_radau5_soltype)
  (const octave_idx_type& NR, const double& XOLD, const double& X,
   const double* Y, const double* CONT, const octave_idx_type* LRC,
   const octave_idx_type& N, OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR, octave_idx_type& IRTRN);


extern "C" {
  F77_RET_T F77_FUNC (radau5, RADAU5)
    (const octave_idx_type& N, odepkg_radau5_usrtype,
     const double& X, const double* Y, const double& XEND,
     const double& H, const double* RTOL, const double* ATOL,
     const octave_idx_type& ITOL, odepkg_radau5_jactype, const octave_idx_type& IJAC,
     const octave_idx_type& MLJAC, const octave_idx_type& MUJAC, odepkg_radau5_masstype,
     const octave_idx_type& IMAS, const octave_idx_type& MLMAS,
     const octave_idx_type& MUMAS, odepkg_radau5_soltype, const octave_idx_type& IOUT,
     const double* WORK, const octave_idx_type& LWORK, const octave_idx_type* IWORK,
     const octave_idx_type& LIWORK, OCTAVE_UNUSED const double* RPAR,
     OCTAVE_UNUSED const octave_idx_type* IPAR, const octave_idx_type& IDID);

  double F77_FUNC (contr5, CONTR5)
    (const octave_idx_type& I, const double& S,
     const double* CONT, const octave_idx_type* LRC);

} // extern "C"

static octave_value_list vradau5extarg;
static octave_value vradau5odefun;
static octave_value vradau5jacfun;
static octave_value vradau5evefun;
static octave_value_list vradau5evesol;
static octave_value vradau5pltfun;
static octave_value vradau5outsel;
static octave_value vradau5refine;
static octave_value vradau5mass;
static octave_value vradau5massstate;
static bool denseOutputStatus;
static Matrix denseOutputMatrix;
static ColumnVector denseTimeVector;
static int denseTimeIndex;

octave_idx_type odepkg_radau5_usrfcn
  (const octave_idx_type& N, const double& X, const double* Y,
   double* F, OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR) {

  // Copy the values that come from the Fortran function element wise,
  // otherwise Octave will crash if these variables will be freed
  ColumnVector A(N);
  for (octave_idx_type vcnt = 0; vcnt < N; vcnt++) {
    A(vcnt) = Y[vcnt];
    //    octave_stdout << "I am here Y[" << vcnt << "] " << Y[vcnt] << std::endl;
    //    octave_stdout << "I am here T " << X << std::endl;
  }

  // Fill the variable for the input arguments before evaluating the
  // function that keeps the set of differential algebraic equations
  octave_value_list varin;
  varin(0) = X; varin(1) = A;
  for (octave_idx_type vcnt = 0; vcnt < vradau5extarg.length (); vcnt++)
    varin(vcnt+2) = vradau5extarg(vcnt);
  octave_value_list vout = feval (vradau5odefun.function_value (), varin, 1);

  // Return the results from the function evaluation to the Fortran
  // solver, again copy them and don't just create a Fortran vector
  ColumnVector vcol = vout(0).column_vector_value ();
  for (octave_idx_type vcnt = 0; vcnt < N; vcnt++)
    F[vcnt] = vcol(vcnt);

  return (true);
}

octave_idx_type odepkg_radau5_jacfcn
  (const octave_idx_type& N, const double& X, const double* Y,
   double* DFY, OCTAVE_UNUSED const octave_idx_type& LDFY,
   OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR) {

  // Copy the values that come from the Fortran function element-wise,
  // otherwise Octave will crash if these variables are freed
  ColumnVector A(N);
  for (octave_idx_type vcnt = 0; vcnt < N; vcnt++)
    A(vcnt) = Y[vcnt];

  // Set the values that are needed as input arguments before calling
  // the Jacobian function and then call the Jacobian interface
  octave_value vt = octave_value (X);
  octave_value vy = octave_value (A);
  octave_value vout = odepkg_auxiliary_evaljacode
    (vradau5jacfun, vt, vy, vradau5extarg);

   Matrix vdfy = vout.matrix_value ();
   for (octave_idx_type vcol = 0; vcol < N; vcol++)
     for (octave_idx_type vrow = 0; vrow < N; vrow++)
       DFY[vrow+vcol*N] = vdfy (vrow, vcol);

  return (true);
}

F77_RET_T odepkg_radau5_massfcn
  (const octave_idx_type& N, double* AM,
   OCTAVE_UNUSED const octave_idx_type* LMAS,
   OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR) {

  // Copy the values that come from the Fortran function element-wise,
  // otherwise Octave will crash if these variables are freed
  ColumnVector A(N);
  for (octave_idx_type vcnt = 0; vcnt < N; vcnt++)
    A(vcnt) = 0.0;

  //  warning_with_id ("OdePkg:InvalidFunctionCall",
  //    "Radau5 can only handle M()=const Mass matrices");

  // Set the values that are needed as input arguments before calling
  // the Jacobian function and then call the Jacobian interface
  octave_value vt = octave_value (0.0);
  octave_value vy = octave_value (A);
  octave_value vout = odepkg_auxiliary_evalmassode
    (vradau5mass, vradau5massstate, vt, vy, vradau5extarg);

  Matrix vam = vout.matrix_value ();
  for (octave_idx_type vrow = 0; vrow < N; vrow++)
    for (octave_idx_type vcol = 0; vcol < N; vcol++)
      AM[vrow+vcol*N] = vam (vrow, vcol);

  return ((F77_RET_T) true);
}

octave_idx_type odepkg_radau5_solfcn
  (const octave_idx_type& NR, const double& XOLD, const double& X,
   const double* Y, const double* CONT, const octave_idx_type* LRC,
   const octave_idx_type& N, OCTAVE_UNUSED const double* RPAR,
   OCTAVE_UNUSED const octave_idx_type* IPAR, octave_idx_type& IRTRN) {

  // Copy the values that come from the Fortran function element-wise,
  // otherwise Octave will crash if these variables are freed
  ColumnVector A(N);
  for (octave_idx_type vcnt = 0; vcnt < N; vcnt++)
    A(vcnt) = Y[vcnt];

  // Set the values that are needed as input arguments before calling
  // the Output function, the solstore function or the Events function
  octave_value vt = octave_value (X);
  octave_value vy = octave_value (A);

  // Check if an 'Events' function has been set by the user
  if (!vradau5evefun.is_empty ()) {
    vradau5evesol = odepkg_auxiliary_evaleventfun
      (vradau5evefun, vt, vy, vradau5extarg, 1);
    if (!vradau5evesol(0).cell_value ()(0).is_empty ())
      if (vradau5evesol(0).cell_value ()(0).int_value () == 1) {
        ColumnVector vttmp = vradau5evesol(0).cell_value ()(2).column_vector_value ();
        Matrix vrtmp = vradau5evesol(0).cell_value ()(3).matrix_value ();
        vt = vttmp.extract (vttmp.numel () - 1, vttmp.numel () - 1);
        vy = vrtmp.extract (vrtmp.rows () - 1, 0, vrtmp.rows () - 1, vrtmp.cols () - 1);
        IRTRN = (vradau5evesol(0).cell_value ()(0).int_value () ? -1 : 0);
      }
  }

  // Save the solutions that come from the Fortran core solver if this
  // is not the initial first call to this function
  if (NR > 1) odepkg_auxiliary_solstore (vt, vy, 1);

  // Check if an 'OutputFcn' has been set by the user (including the
  // values of the options for 'OutputSel' and 'Refine')
  if (!vradau5pltfun.is_empty ()) {
    printf("start solfcn-pltfcn\n");
    if (vradau5refine.int_value () > 0) {
      ColumnVector B(N); double vtb = 0.0;
      for (octave_idx_type vcnt = 1; vcnt < vradau5refine.int_value (); vcnt++) {

        // Calculate time stamps between XOLD and X and get the
        // results at these time stamps
        vtb = (X - XOLD) * vcnt / vradau5refine.int_value () + XOLD;
        for (octave_idx_type vcou = 0; vcou < N; vcou++)
          B(vcou) = F77_FUNC (contr5, CONTR5) (vcou+1, vtb, CONT, LRC);

        // Evaluate the 'OutputFcn' with the approximated values from
        // the F77_FUNC before the output of the results is done
        octave_value vyr = octave_value (B);
        octave_value vtr = octave_value (vtb);
        odepkg_auxiliary_evalplotfun
          (vradau5pltfun, vradau5outsel, vtr, vyr, vradau5extarg, 1);
      }
    }
    // Evaluate the 'OutputFcn' with the results from the solver, if
    // the OutputFcn returns true then set a negative value in IRTRN
    IRTRN = - odepkg_auxiliary_evalplotfun
      (vradau5pltfun, vradau5outsel, vt, vy, vradau5extarg, 1);
  }

  // Check if dense output is required. If yes, calculate the solution at the given points
  if (denseOutputStatus == true) {
    for (octave_idx_type idxDen = denseTimeIndex; idxDen < denseTimeVector.numel(); idxDen++)
    {
      // TODO: Maybe there is a more efficient way to evaluate this
      if (denseTimeVector(idxDen) >= XOLD && denseTimeVector(idxDen) <= X)
      {
        for (octave_idx_type vcou = 0; vcou < N; vcou++)
        {
          // If value is found then evaluate with the contr5 function
          denseOutputMatrix(idxDen, vcou)=F77_FUNC (contr5, CONTR5) (vcou+1, denseTimeVector(idxDen), CONT, LRC);
        }
        denseTimeIndex=idxDen;
      }
      else
      {
        // Do nothing, no dense point found in current interval
      }
    }
  }
  else
  {
    // Dense output not activated ...
  }

  return (true);
}

DEFUN_DLD (ode5r, args, nargout,
"-*- texinfo -*-\n\
@deftypefn  {Command} {[@var{}] =} ode5r (@var{fun}, @var{trange}, @var{init}, [@var{ode_opt}], [@var{par1}, @var{par2}, @dots{}])\n\
@deftypefnx {Command} {[@var{sol}] =} ode5r (@var{fun}, @var{trange}, @var{init}, [@var{ode_opt}], [@var{par1}, @var{par2}, @dots{}])\n\
@deftypefnx {Command} {[@var{t}, @var{y}, [@var{xe}, @var{ye}, @var{ie}]] =} ode5r (@var{fun}, @var{trange}, @var{init}, [@var{ode_opt}], [@var{par1}, @var{par2}, @dots{}])\n\
\n\
This function file can be used to solve a set of stiff ordinary differential equations (ODEs) and stiff differential algebraic equations (DAEs). @code{ode5r} is a wrapper to Hairer's and Wanner's Fortran solver @file{radau5.f}. The method is based on the 3-stage Radau collocation method. It solves DAEs of the form @code{Mx' = f(t,y)}, where @code{M} is a constant, square matrix which may be singular. The code is applicable to problems of index 1,2,3. The higher-index variables must be identified by the user.\n\
\n\
@var{fun} is a function handle, inline function, or string containing the name of the function that defines the ODE: @code{y' = f(t,y)}. The function must accept two inputs where the first is time @var{t} and the second is a column vector of unknowns @var{y}.\n\
\n\
@var{trange} specifies the time interval over which the ODE will be evaluated, @var{y0} contains the initial values of the states, @var{dy0} contains the initial values of the derivatives.\n\
\n\
The optional fourth argument @var{ode_opt} specifies non-default options to the ODE solver. It is a structure generated by @code{odeset}.\n\
\n\
If this function is called with no return argument then it plots the solution over time in a figure window while solving the set of IDEs that are defined in a function and specified by the function handle @var{fun}.\n\
\n\
The function typically returns two outputs. Variable @var{t} is a column vector and contains the times where the solution was computed. The output @var{y} is a matrix in which each column refers to a different unknown of the problem and each row corresponds to a time in @var{t}. If @var{trange} specifies intermediate time steps, only those will be returned.\n\
\n\
The output can also be returned as a structure @var{solution} which has a field @var{x} containing a row vector of times where the solution was evaluated and a field @var{y} containing the solution matrix such that each column corresponds to a time in @var{x}.\n\
\n\
For example,\n\
@example\n\
function y = odepkg_equations_lorenz (t, x)\n\
  y = [10 * (x(2) - x(1));\n\
       x(1) * (28 - x(3));\n\
       x(1) * x(2) - 8/3 * x(3)];\n\
endfunction\n\
\n\
vopt = odeset (\"InitialStep\", 1e-3, \"MaxStep\", 1e-1, \\\n\
               \"OutputFcn\", @@odephas3, \"Refine\", 5);\n\
ode5r (@@odepkg_equations_lorenz, [0, 25], [3 15 1], vopt);\n\
@end example\n\
@seealso{odepkg}\n\
@end deftypefn") {

  octave_idx_type nargin = args.length (); // The number of input arguments
  octave_value_list vretval;               // The cell array of return args
  octave_scalar_map tmpopt, vodeopt;       // The OdePkg options structure

  // Check number and types of all input arguments
  if (nargin < 3) {
    print_usage ();
    return (vretval);
  }

  // If args(0)==function_handle is valid then set the vradau5odefun
  // variable that has been defined "static" before
  if (!args(0).is_function_handle () && !args(0).is_inline_function ()) {
    error_with_id ("OdePkg:InvalidArgument",
      "First input argument must be a valid function handle");
    return (vretval);
  }
  else // We store the args(0) argument in the static variable vradau5odefun
    vradau5odefun = args(0);

  // Check if the second input argument is a valid vector describing
  // the time window that should be solved, it may be of length 2 ONLY
  if (args(1).is_scalar_type () || !odepkg_auxiliary_isvector (args(1))) {
    error_with_id ("OdePkg:InvalidArgument",
      "Second input argument must be a valid vector");
    return (vretval);
  }

  // Check if the thirt input argument is a valid vector describing
  // the initial values of the variables of the differential equations
  if (!odepkg_auxiliary_isvector (args(2))) {
    error_with_id ("OdePkg:InvalidArgument",
      "Third input argument must be a valid vector");
    return (vretval);
  }
  
  // Option structure and etra arguments
  if (nargin >= 4) {
    // Fifth input argument != OdePkg option, need a default structure
    if (!args(3).is_map ()) {
      octave_value_list tmp = feval ("odeset", tmp, 1);
      tmpopt = tmp(0).scalar_map_value ();
      for (octave_idx_type vcnt = 3; vcnt < nargin; vcnt++){
        vradau5extarg(vcnt-3) = args(vcnt); // Save arguments in vddaskrextarg
      }
    }
    // Fifth input argument == OdePkg option, extra input args given too
    else if (nargin > 4) {
      octave_value_list varin;
      tmpopt = args(3).scalar_map_value ();
      for (octave_idx_type vcnt = 4; vcnt < nargin; vcnt++)
        vradau5extarg(vcnt-4) = args(vcnt); // Save extra arguments
    }
    // Fifth input argument == OdePkg option, no extra input args given
    else {
      tmpopt = args(3).scalar_map_value ();
    }
  }
  else { // if nargin == 4, everything else has been checked before
    octave_value_list tmp = feval ("odeset", tmp, 1);
    tmpopt = tmp(0).scalar_map_value (); // Create a default structure
  }

  // Create default option structure
  octave_value_list varin;
  varin(0) = args(2).array_value ().size (0);
  varin(1) = args(1).array_value ()(0); // init time
  varin(2) = args(1).array_value ()(args(2).length () - 1); // end time
  octave_value_list defaults = feval ("odedefaults", varin);

  // FIXME: Remove NormCotrol, NonNegative, JPattern, Vetorized, MStateDependence, MVPattern,
  // MassSingular, InitialSlope, BDF

  // Merge defaults and create final option structure
  varin(0) = "ode5r";
  varin(1) = tmpopt;
  varin(2) = defaults(0).scalar_map_value ();
  varin(3) = defaults(1).scalar_map_value ();
  varin(4) = defaults(2).scalar_map_value ();
  octave_value_list tmp = feval ("odemergeopts", varin);
  vodeopt = tmp(0).scalar_map_value ();

/* Start PREPROCESSING, ie. check which options have been set and
 * print warnings if there are options that can't be handled by this
 * solver or have not been implemented yet
 *******************************************************************/

  // Setting the tolerance type that depends on the types (scalar or
  // vector) of the options RelTol and AbsTol
  octave_idx_type vitol = 0;
  if (vodeopt.contents("RelTol").is_scalar_type () && (vodeopt.contents("RelTol").length () == vodeopt.contents("AbsTol").length ()))
    vitol = 0;
  else if (!vodeopt.contents("RelTol").is_scalar_type () && (vodeopt.contents("RelTol").length () == vodeopt.contents("AbsTol").length ()))
    vitol = 1;
  else {
    error_with_id ("OdePkg:InvalidOption",
      "Values of \"RelTol\" and \"AbsTol\" must have same length");
    return (vretval);
  }

  vradau5pltfun = vodeopt.contents ("OutputFcn");
  if (vradau5pltfun.is_empty () && nargout == 0) vradau5pltfun = "odeplot";

  // Implementation of dense output has been finished.
  // Check if length of the specified input time trange is more than 2 and
  // activate it if yes.
  denseTimeIndex = 0;
  ColumnVector tSpan (2);
  denseTimeVector = args(1).column_vector_value ();
  if (!(denseTimeVector.is_sorted () == ASCENDING)) {
    error_with_id ("OdePkg:InvalidArgument",
      "The time trange vector must be sorted in ascending order");
  }
  tSpan(0) = denseTimeVector(0);
  tSpan(1) = denseTimeVector(denseTimeVector.numel () - 1);

  if (denseTimeVector.numel () > 2) {
    denseOutputStatus = true;
    denseOutputMatrix.resize (denseTimeVector.numel (), args(2).length (), 0);
  }
  else {
    denseOutputStatus = false;
  }

  if (vodeopt.contents ("InitialStep").is_empty ()) {
    vodeopt.contents("InitialStep") = 1.0e-6;
    warning_with_id ("OdePkg:InvalidOption",
      "Option \"InitialStep\" not set, new value %3.1e is used",
      vodeopt.contents ("InitialStep").double_value ());
  }

  // Implementation of the option 'Jacobian' has been finished, these
  // options can be set by the user to another value than default
  vradau5jacfun = vodeopt.contents ("Jacobian");
  octave_idx_type vradau5jac = 0; // We need to set this if no Jac available
  if (!vradau5jacfun.is_empty ()) vradau5jac = 1;

  vradau5outsel = vodeopt.contents ("OutputSel");
  vradau5refine = vodeopt.contents ("Refine");
  vradau5evefun = vodeopt.contents ("Events");

  // Implementation of the option 'Mass' has been finished, these
  // options can be set by the user to another value than default
  vradau5mass = vodeopt.contents ("Mass");
  octave_idx_type vradau5mas = 0;
  if (!vradau5mass.is_empty ()) {
    vradau5mas = 1;
    if (vradau5mass.is_function_handle () || vradau5mass.is_inline_function ())
      warning_with_id ("OdePkg:InvalidOption",
        "Option \"Mass\" only supports constant mass matrices M() and not M(t,y)");
  }

  // The option MStateDependence will be ignored by this solver, the
  // core Fortran solver doesn't support this option
  vradau5massstate = "none";

  if (!vodeopt.isfield ("NewtonTol")
      || vodeopt.contents ("NewtonTol").is_empty ()) {
    vodeopt.assign ("NewtonTol", 1e-8);
    warning_with_id ("OdePkg:InvalidOption",
      "Option \"NewtonTol\" not set, default value 1e-8 is used");
  }

  if (!vodeopt.isfield ("MaxNewtonIterations")
      || vodeopt.contents ("MaxNewtonIterations").is_empty ()) {
    vodeopt.assign ("MaxNewtonIterations", 7);
    warning_with_id ("OdePkg:InvalidOption",
      "Option \"MaxNewtonIterations\" not set, default value 7 is used");
  }

/* Start MAINPROCESSING, set up all variables that are needed by this
 * solver and then initialize the solver function and get into the
 * main integration loop
 ********************************************************************/
  NDArray vY0 = args(2).array_value ();
  NDArray vRTOL = vodeopt.contents("RelTol").array_value ();
  NDArray vATOL = vodeopt.contents("AbsTol").array_value ();

  octave_idx_type N = args(2).length ();
  double X = tSpan(0);
  double* Y = vY0.fortran_vec ();
  double XEND = tSpan(1);
  double H = vodeopt.contents("InitialStep").double_value ();
  double *RTOL = vRTOL.fortran_vec ();
  double *ATOL = vATOL.fortran_vec ();
  octave_idx_type ITOL = vitol;
  octave_idx_type IJAC = vradau5jac;
  octave_idx_type MLJAC=N;
  octave_idx_type MUJAC=N;

  octave_idx_type IMAS=vradau5mas;
  octave_idx_type MLMAS=N;
  octave_idx_type MUMAS=N;
  octave_idx_type IOUT = 1; // The SOLOUT function will always be called
  octave_idx_type LWORK = N*(N+N+7*N+3*7+3)+20;
  OCTAVE_LOCAL_BUFFER (double, WORK, LWORK);
  for (octave_idx_type vcnt = 0; vcnt < LWORK; vcnt++) WORK[vcnt] = 0.0;
  octave_idx_type LIWORK = (2+(7-1)/2)*N+20;
  OCTAVE_LOCAL_BUFFER (octave_idx_type, IWORK, LIWORK);
  for (octave_idx_type vcnt = 0; vcnt < LIWORK; vcnt++) IWORK[vcnt] = 0;
  double RPAR[1] = {0.0};
  octave_idx_type IPAR[1] = {0};
  octave_idx_type IDID = 0;

  IWORK[0] = 1;  // Switch for transformation of Jacobian into Hessenberg form
  IWORK[2] = vodeopt.contents ("MaxNewtonIterations").int_value ();     // Maximum number of Newton iterations
  WORK[2]  = -1; // Recompute Jacobian after every succesful step
  WORK[3]  = vodeopt.contents ("NewtonTol").double_value (); // Tolerance of Newton iteration
  WORK[6]  = vodeopt.contents("MaxStep").double_value (); // Set the maximum step size

  // Check if the user has set some of the options "OutputFcn", "Events"
  // etc. and initialize the plot, events and the solstore functions
  octave_value vtim = tSpan(0);
  octave_value vsol = args(2);
  odepkg_auxiliary_solstore (vtim, vsol, 0);
  if (!vradau5pltfun.is_empty ()) odepkg_auxiliary_evalplotfun
    (vradau5pltfun, vradau5outsel, octave_value(tSpan), args(2), vradau5extarg, 0);
  if (!vradau5evefun.is_empty ())
    odepkg_auxiliary_evaleventfun (vradau5evefun, vtim, args(2), vradau5extarg, 0);

  // octave_stdout <<  "X VALUE=" << X << XEND << std::endl;
  // We are calling the core solver and solve the set of ODEs or DAEs
  F77_XFCN (radau5, RADAU5, // Keep 5 arguments per line here
            (N, odepkg_radau5_usrfcn, X, Y,
             XEND, H, RTOL, ATOL, ITOL,
             odepkg_radau5_jacfcn, IJAC, MLJAC, MUJAC, odepkg_radau5_massfcn,
             IMAS, MLMAS, MUMAS, odepkg_radau5_solfcn, IOUT,
             WORK, LWORK, IWORK, LIWORK, RPAR,
             IPAR, IDID));

  if (IDID < 0) {
    // odepkg_auxiliary_mebdfanalysis (IDID);
    error_with_id ("hugh:hugh", "missing implementation");
    vretval(0) = 0.0;
    return (vretval);
  }

/* Start POSTPROCESSING, check how many arguments should be returned
 * to the caller and check which extra arguments have to be set
 *******************************************************************/

  // Return the results that have been stored in the
  // odepkg_auxiliary_solstore function
  octave_value vtres, vyres;
  odepkg_auxiliary_solstore (vtres, vyres, 2);

  // Set up variables to make it possible to call the cleanup
  // functions of 'OutputFcn' and 'Events' if any
  Matrix vlastline;
  vlastline = vyres.matrix_value ();
  vlastline = vlastline.extract (vlastline.rows () - 1, 0,
                                 vlastline.rows () - 1, vlastline.cols () - 1);
  octave_value vted = octave_value (XEND);
  octave_value vfin = octave_value (vlastline);

  if (!vradau5pltfun.is_empty ()) odepkg_auxiliary_evalplotfun
    (vradau5pltfun, vradau5outsel, vted, vfin, vradau5extarg, 2);
  if (!vradau5evefun.is_empty ()) odepkg_auxiliary_evaleventfun
    (vradau5evefun, vted, vfin, vradau5extarg, 2);

  // Get the stats information as an octave_scalar_map if the option 'Stats'
  // has been set with odeset
  // "nsteps", "nfailed", "nfevals", "npds", "ndecomps", "nlinsols"
  octave_value_list vstatinput;
  vstatinput(0) = IWORK[16];
  vstatinput(1) = IWORK[17];
  vstatinput(2) = IWORK[13];
  vstatinput(3) = IWORK[14];
  vstatinput(4) = IWORK[18];
  vstatinput(5) = IWORK[19];
  octave_value vstatinfo;
  if ((vodeopt.contents("Stats").string_value ().compare ("on") == 0) && (nargout == 1))
    vstatinfo = odepkg_auxiliary_makestats (vstatinput, false);
  else if ((vodeopt.contents("Stats").string_value ().compare ("on") == 0) && (nargout != 1))
    vstatinfo = odepkg_auxiliary_makestats (vstatinput, true);

  // Set up output arguments that depend on how many output arguments
  // are desired from the caller
  if (nargout == 1) {
    octave_scalar_map vretmap;
    if (denseOutputStatus==true) {
      vretmap.assign ("x", octave_value(denseTimeVector));
      vretmap.assign ("y", octave_value(denseOutputMatrix));
    }
    else {
      vretmap.assign ("x", vtres);
      vretmap.assign ("y", vyres);
    }
    vretmap.assign ("solver", "ode5r");
    if (!vstatinfo.is_empty ()) // Event implementation
      vretmap.assign ("stats", vstatinfo);
    if (!vradau5evefun.is_empty ()) {
      vretmap.assign ("ie", vradau5evesol(0).cell_value ()(1));
      vretmap.assign ("xe", vradau5evesol(0).cell_value ()(2));
      vretmap.assign ("ye", vradau5evesol(0).cell_value ()(3));
    }
    vretval(0) = octave_value (vretmap);
  }
  else if (nargout == 2) {
    if (denseOutputStatus==true) {
      vretval(0) = octave_value(denseTimeVector);
      vretval(1) = octave_value(denseOutputMatrix);
    }
    else {
      vretval(0) = vtres;
      vretval(1) = vyres;
    }
  }
  else if (nargout == 5) {
    Matrix vempty; // prepare an empty matrix
    if (denseOutputStatus==true) {
      vretval(0) = octave_value(denseTimeVector);
      vretval(1) = octave_value(denseOutputMatrix);
    }
    else {
      vretval(0) = vtres;
      vretval(1) = vyres;
    }
    vretval(2) = vempty;
    vretval(3) = vempty;
    vretval(4) = vempty;
    if (!vradau5evefun.is_empty ()) {
      vretval(2) = vradau5evesol(0).cell_value ()(2);
      vretval(3) = vradau5evesol(0).cell_value ()(3);
      vretval(4) = vradau5evesol(0).cell_value ()(1);
    }
  }

  // Clear matrix for dense output
  denseOutputMatrix.clear();

  return (vretval);
}

/*
%! # We are using the "Van der Pol" implementation for all tests that
%! # are done for this function. We also define a Jacobian, Events,
%! # pseudo-Mass implementation. For further tests we also define a
%! # reference solution (computed at high accuracy) and an OutputFcn
%!function [ydot] = fpol (vt, vy, varargin) %# The Van der Pol
%!  ydot = [vy(2); (1 - vy(1)^2) * vy(2) - vy(1)];
%!function [vjac] = fjac (vt, vy, varargin) %# its Jacobian
%!  vjac = [0, 1; -1 - 2 * vy(1) * vy(2), 1 - vy(1)^2];
%!function [vjac] = fjcc (vt, vy, varargin) %# sparse type
%!  vjac = sparse ([0, 1; -1 - 2 * vy(1) * vy(2), 1 - vy(1)^2]);
%!function [vval, vtrm, vdir] = feve (vt, vy, varargin)
%!  vval = fpol (vt, vy, varargin); %# We use the derivatives
%!  vtrm = zeros (2,1);             %# that's why component 2
%!  vdir = ones (2,1);              %# seems to not be exact
%!function [vval, vtrm, vdir] = fevn (vt, vy, varargin)
%!  vval = fpol (vt, vy, varargin); %# We use the derivatives
%!  vtrm = ones (2,1);              %# that's why component 2
%!  vdir = ones (2,1);              %# seems to not be exact
%!function [vmas] = fmas (vt, vy)
%!  vmas =  [1, 0; 0, 1];     %# Dummy mass matrix for tests
%!function [vmas] = fmsa (vt, vy)
%!  vmas = sparse ([1, 0; 0, 1]); %# A sparse dummy matrix
%!function [vref] = fref ()   %# The computed reference solut
%!  vref = [0.32331666704577, -1.83297456798624];
%!function [vout] = fout (vt, vy, vflag, varargin)
%!  if (regexp (char (vflag), 'init') == 1)
%!    if (size (vt) != [2, 1] && size (vt) != [1, 2])
%!      error ('"fout" step "init"');
%!    end
%!  elseif (isempty (vflag))
%!    if (size (vt) ~= [1, 1]) error ('"fout" step "calc"'); end
%!    vout = false;
%!  elseif (regexp (char (vflag), 'done') == 1)
%!    if (size (vt) ~= [1, 1]) error ('"fout" step "done"'); end
%!  else error ('"fout" invalid vflag');
%!  end
%!
%! %# Turn off output of warning messages for all tests, turn them on
%! %# again if the last test is called
%!error %# input argument number one
%!  warning ('off', 'OdePkg:InvalidOption');
%!  B = ode5r (1, [0 25], [3 15 1]);
%!error %# input argument number two
%!  B = ode5r (@fpol, 1, [3 15 1]);
%!error %# input argument number three
%!  B = ode5r (@flor, [0 25], 1);
%!test %# dense output with one output argument
%!  vsol = ode5r (@fpol, [0:0.1:2], [2 0]);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!  assert (isfield (vsol, 'solver'));
%!  assert (vsol.solver, 'ode5r');
%!  assert (vsol.x, [0:0.1:2]');
%!  assert (size(vsol.y), [length(vsol.x), 2]);
%!test %# dense output with two output argument
%!  [vt, vy] = ode5r (@fpol, [0:0.1:2], [2 0]);
%!  assert ([vt(end), vy(end,:)], [2, fref], 1e-3);
%!  assert (vt, [0:0.1:2]');
%!  assert (size(vy), [length(vt), 2]);
%!test %# dense output with five output argument
%!  [vt, vy, vxe, vye, vie] = ode5r (@fpol, [0:0.1:2], [2 0]);
%!  assert ([vt(end), vy(end,:)], [2, fref], 1e-3);
%!  assert (vt, [0:0.1:2]');
%!  assert (size(vy), [length(vt), 2]);
%!  assert ([vie, vxe, vye], []);
%!test %# one output argument
%!  vsol = ode5r (@fpol, [0 2], [2 0]);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!  assert (isfield (vsol, 'solver'));
%!  assert (vsol.solver, 'ode5r');
%!test %# two output arguments
%!  [vt, vy] = ode5r (@fpol, [0 2], [2 0]);
%!  assert ([vt(end), vy(end,:)], [2, fref], 1e-3);
%!test %# five output arguments and no Events
%!  [vt, vy, vxe, vye, vie] = ode5r (@fpol, [0 2], [2 0]);
%!  assert ([vt(end), vy(end,:)], [2, fref], 1e-3);
%!  assert ([vie, vxe, vye], []);
%!test %# anonymous function instead of real function
%!  fvdb = @(vt,vy) [vy(2); (1 - vy(1)^2) * vy(2) - vy(1)];
%!  vsol = ode5r (fvdb, [0 2], [2 0]);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# extra input arguments passed trhough
%!  vsol = ode5r (@fpol, [0 2], [2 0], 12, 13, 'KL');
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# empty OdePkg structure *but* extra input arguments
%!  vopt = odeset;
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt, 12, 13, 'KL');
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!error %# order of time vector
%!  vsol = ode5r (@fpol, [0 0.7 0.5 0.1 2 1.5], [2 0]);
%!test %# AbsTol option
%!  vopt = odeset ('AbsTol', 1e-5);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# AbsTol and RelTol option
%!  vopt = odeset ('AbsTol', 1e-8, 'RelTol', 1e-8);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# RelTol and NormControl option -- higher accuracy
%!  vopt = odeset ('RelTol', 1e-8, 'NormControl', 'on');
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-6);
%!test %# Keeps initial values while integrating
%!  vopt = odeset ('NonNegative', 2);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-6);
%!test %# Details of OutputSel and Refine can't be tested
%!  vopt = odeset ('OutputFcn', @fout, 'OutputSel', 1, 'Refine', 5);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!test %# Stats must add further elements in vsol
%!  vopt = odeset ('Stats', 'on');
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert (isfield (vsol, 'stats'));
%!  assert (isfield (vsol.stats, 'nsteps'));
%!test %# InitialStep option
%!  vopt = odeset ('InitialStep', 1e-8);
%!  vsol = ode5r (@fpol, [0 0.2], [2 0], vopt);
%!  assert ([vsol.x(2)-vsol.x(1)], [1e-8], 1e-9);
%!test %# MaxStep option
%!  vopt = odeset ('MaxStep', 1e-2);
%!  vsol = ode5r (@fpol, [0 0.2], [2 0], vopt);
%!  assert ([vsol.x(5)-vsol.x(4)], [1e-2], 1e-2);
%!test %# Events option add further elements in vsol
%!  vopt = odeset ('Events', @feve);
%!  vsol = ode5r (@fpol, [0 10], [2 0], vopt);
%!  assert (isfield (vsol, 'ie'));
%!  assert (vsol.ie(1), 2);
%!  assert (isfield (vsol, 'xe'));
%!  assert (isfield (vsol, 'ye'));
%!test %# Events option, now stop integration
%!  warning ('off', 'OdePkg:HideWarning');
%!  vopt = odeset ('Events', @fevn, 'NormControl', 'on');
%!  vsol = ode5r (@fpol, [0 10], [2 0], vopt);
%!  assert ([vsol.ie, vsol.xe, vsol.ye], ...
%!    [2.0, 2.496110, -0.830550, -2.677589], 1e-1);
%!test %# Events option, five output arguments
%!  vopt = odeset ('Events', @fevn, 'NormControl', 'on');
%!  [vt, vy, vxe, vye, vie] = ode5r (@fpol, [0 10], [2 0], vopt);
%!  assert ([vie, vxe, vye], ...
%!    [2.0, 2.496110, -0.830550, -2.677589], 1e-1);
%!  warning ('on', 'OdePkg:HideWarning');
%!test %# Jacobian option
%!  vopt = odeset ('Jacobian', @fjac);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# Jacobian option and sparse return value
%!  vopt = odeset ('Jacobian', @fjcc);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!
%! %# test for JPattern option is missing
%! %# test for Vectorized option is missing
%!
%!test %# Mass option as function
%!  vopt = odeset ('Mass', @fmas);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# Mass option as matrix
%!  vopt = odeset ('Mass', eye (2,2));
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# Mass option as sparse matrix
%!  vopt = odeset ('Mass', sparse (eye (2,2)));
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# Mass option as function and sparse matrix
%!  vopt = odeset ('Mass', @fmsa);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# Mass option as function and MStateDependence
%!  vopt = odeset ('Mass', @fmas, 'MStateDependence', 'strong');
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!
%! %# test for MvPattern option is missing
%! %# test for InitialSlope option is missing
%! %# test for MaxOrder option is missing
%!
%!test %# Set BDF option to something else than default
%!  vopt = odeset ('BDF', 'on');
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# Set NewtonTol option to something else than default
%!  vopt = odeset ('NewtonTol', 1e-3);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!test %# Set MaxNewtonIterations option to something else than default
%!  vopt = odeset ('MaxNewtonIterations', 2);
%!  vsol = ode5r (@fpol, [0 2], [2 0], vopt);
%!  assert ([vsol.x(end), vsol.y(end,:)], [2, fref], 1e-3);
%!
%!  warning ('on', 'OdePkg:InvalidOption');
*/

/*
;;; Local Variables: ***
;;; mode: C++ ***
;;; End: ***
*/
